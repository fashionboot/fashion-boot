export const environment = {

  ORDEM_ID: String,
  production: true,
  baseUrl: 'https://fashion-boot-prod.herokuapp.com',
  keycloak: {
    // Url of the Identity Provider
    issuer: 'https://key.mxti.com.br/auth',
    // Realm
    realm: 'fashionboot',
    clientId: 'fashionbootfront',
  },
};
