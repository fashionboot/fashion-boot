import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FrontPageComponent } from './front-page/front-page.component';
import { CategoryComponent } from './category/category.component';
import { CategoryListComponent } from './category/category-list/category-list.component';
import { MarcaListComponent } from './category/marca-list/marca-list.component';
import { ProductDetailComponent } from './category/product-detail/product-detail.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { EmptyComponent } from './empty/empty.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PaymentComponent } from './payment/payment.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {DeliveryComponent} from './delivery/delivery.component';
import {MenuPerfilComponent} from './menu-perfil/menu-perfil.component';
import {PerfilSidebarComponent} from './menu-perfil/perfil-sidebar/perfil-sidebar.component';
import {AccountInformationComponent} from './menu-perfil/account/account-information/account-information.component';
import {AccountEditComponent} from './menu-perfil/account/account-edit/account-edit.component';
import {PaymentsInformationComponent} from './menu-perfil/payments/payments-information/payments-information.component';
import {PaymentsEditComponent} from './menu-perfil/payments/payments-edit/payments-edit.component';
import {AuthGuard} from './guard/auth-guard.service';
import {PedidoListComponent} from './menu-perfil/pedido/pedido-list/pedido-list.component';
import {SearchProductsComponent} from './category/search-products/search-products.component';
import {ItemOrderedComponent} from './menu-perfil/item-ordered/item-ordered.component';



const routes: Routes = [
 // { path: 'login', component: LoginComponent, canActivate: [AuthGuard]},
 // { path: 'register', component: RegisterComponent },
  { path: 'menu', component: MenuPerfilComponent },
  { path: 'register/delivery', component: DeliveryComponent },
  { path: '', pathMatch: 'full', component: FrontPageComponent},
  { path: 'category',component: CategoryComponent,
    children: [
      { path: ':category', component: CategoryListComponent },
      { path: 'product/:id', component: ProductDetailComponent },
      { path: 'marca/:marca', component: MarcaListComponent },
      { path: 'produto/:produto', component: SearchProductsComponent },

    ]
  },
  { path: 'menu-perfil',component: MenuPerfilComponent, canActivate: [AuthGuard],
    children: [
      {path: 'pedidos', component: PedidoListComponent},
      {path: 'pedidos/itens', component: ItemOrderedComponent},
      { path: 'payments', component: PaymentsInformationComponent },
      { path: 'perfil-sidebar', component: PerfilSidebarComponent },
      { path: 'payments-edit', component: PaymentsEditComponent },
      { path: 'account-information', component: AccountInformationComponent },
      { path: 'account-edit', component: AccountEditComponent },
    ]
  },
  { path: 'shopping-cart', component: ShoppingCartComponent },
  { path: 'payment', component: PaymentComponent, canActivate: [AuthGuard] },
  { path: 'empty', component: EmptyComponent },

  { path: '**', component: NotFoundComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false, useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
