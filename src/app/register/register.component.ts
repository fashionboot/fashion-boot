import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Ordem } from '../interface/ec-template.interface';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  data: Ordem;
  form: FormGroup;

  constructor(private dataService: DataService, private fb: FormBuilder, private router: Router) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      usuarioInfo: this.fb.group({
        name: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required],
        phone: ['', Validators.required]
      })
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.data.usuario = this.form.get('usuarioInfo').value;
      this.dataService.submitOrder(this.data);
    } else {
      this.validateAllFormFields(this.form);
      this.scrollToTop();
    }
  }


  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  isRequiredValid(field: string) {
    return this.form.get(field).hasError('required') && this.form.get(field).touched;
  }

  isMinLengthValid(field: string) {
    return this.form.get(field).hasError('minlength') && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  toggleDeliveryCB(isChecked: boolean) {
    const recipientName = this.form.get('addressInfo.road');
    const recipientNumber = this.form.get('addressInfo.number');
    if (isChecked) {
      recipientName.setValue(this.form.get('usuarioInfo.name').value);
      recipientNumber.setValue(this.form.get('usuarioInfo.number').value);
    } else {
      recipientName.reset();
      recipientNumber.reset();
    }
  }

  // mark all field as touched
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  limitedInputLength(field: string, limit: number) {
    const formControl = this.form.get(field);
    if (formControl.value.length > limit) {
      formControl.setValue(formControl.value.slice(0, limit));
    }
  }





  scrollToTop() {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 6);
  }


}
