import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MenuPerfilComponent} from './menu-perfil.component';
import {PerfilSidebarComponent} from './perfil-sidebar/perfil-sidebar.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AccountEditComponent } from './account/account-edit/account-edit.component';
import {AccountInformationComponent} from './account/account-information/account-information.component';
import { PaymentsInformationComponent } from './payments/payments-information/payments-information.component';
import { PaymentsEditComponent } from './payments/payments-edit/payments-edit.component';
import { PedidoListComponent } from './pedido/pedido-list/pedido-list.component';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import { ItemOrderedComponent } from './item-ordered/item-ordered.component';

@NgModule({
  declarations: [
    MenuPerfilComponent,
    PerfilSidebarComponent,
    PaymentsInformationComponent,
    AccountInformationComponent,
    AccountEditComponent,
    PaymentsInformationComponent,
    PaymentsEditComponent,
    PedidoListComponent,
    ItemOrderedComponent,

  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgbPaginationModule,
  ],
  exports:[
    MenuPerfilComponent,
    PerfilSidebarComponent,
    PaymentsInformationComponent,
  ]
})
export class MenuPerfilModule { }
