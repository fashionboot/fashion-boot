import {Component, OnInit} from '@angular/core';
import {Address, City, Ordem, State, Usuario} from '../../../interface/ec-template.interface';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../services/data.service';
import {Router} from '@angular/router';
import {CidadeService} from '../../../services/cidade.service';
import {EstadoService} from '../../../services/estado.service';
import {UsuarioService} from '../../../services/usuario.service';
import {AuthService} from '../../../guard/auth.service';

@Component({
    selector: 'app-account-information',
    templateUrl: './account-information.component.html',
    styleUrls: ['./account-information.component.scss']
})
export class AccountInformationComponent implements OnInit {

    data: Ordem;
    // @ts-ignore
    city: City = {
        id: '',
    };
    form: FormGroup;
    cidade: City[] = [];
    estados: State[] = [];
    cidadeSelecionada = '';
    estadoSelecionada = 'Default';


    address: Address = {
        cep: '', city: undefined, complement: '', number: '', road: '',
        id: '', cityName :'',
    };


    usuario: Usuario = <Usuario>{
        sub: '',
        name: '',
        email: '',
        phone: '',
        address: this.address

    };


    constructor(private dataService: DataService,
                private fb: FormBuilder,
                private router: Router,
                private cidadeService: CidadeService,
                private estadoService: EstadoService,
                private usuarioService: UsuarioService,
                private authService: AuthService
    ) {

    }

   ngOnInit() {
        this.usuarioService.findUsuarioById(this.authService.getLoggedUser().sub).subscribe(
            respota => {
                this.usuario = respota;
                if(this.usuario.address == null){
                    this.usuario.address = this.address;
                }

                this.form = this.fb.group({
                    usuarioInfo: this.fb.group({
                        name: [this.authService.getLoggedUser().name || ''],
                        email: [this.authService.getLoggedUser().email || ''],
                        number: [this.usuario.phone || '']
                    }),
                    addressInfo: this.fb.group({
                        road: [this.usuario.address.road || ''],
                        number: [this.usuario.address.number || ''],
                        complement: [this.usuario.address.complement || ''],
                        district: [this.usuario.address.district || ''],
                        cep: [this.usuario.address.cep || ''],
                    })
                });
                if(this.usuario.address.city != null){
                    this.estadoSelecionada = this.usuario.address.city.state.id;
                    this.cidadeSelecionada = this.usuario.address.city.id;
                    this.listarEstados();
                }

            }
        );


        this.estadoService.findAllEstados().subscribe(res => {
            this.estados = res;
        });

    }


    isFieldValid(field: string) {
        return !this.form.get(field).valid && this.form.get(field).touched;
    }

    isRequiredValid(field: string) {
        return this.form.get(field).hasError('required') && this.form.get(field).touched;
    }

    isMinLengthValid(field: string) {
        return this.form.get(field).hasError('minlength') && this.form.get(field).touched;
    }

    displayFieldCss(field: string) {
        return {
            'has-error': this.isFieldValid(field),
            'has-feedback': this.isFieldValid(field)
        };
    }

    // mark all field as touched
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({onlySelf: true});
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }


    // Control input's limit characters
    limitedInputLength(field: string, limit: number) {
        const formControl = this.form.get(field);
        if (formControl.value.length > limit) {
            formControl.setValue(formControl.value.slice(0, limit));
        }
    }


    scrollToTop() {
        const scrollToTop = window.setInterval(() => {
            const pos = window.pageYOffset;
            if (pos > 0) {
                window.scrollTo(0, pos - 20); // how far to scroll on each step
            } else {
                window.clearInterval(scrollToTop);
            }
        }, 6);
    }

    teste() {
        console.log(this.cidadeSelecionada);
    }

    controlEstado = new FormControl(this.estadoSelecionada);
    controlCidade = new FormControl(this.controlCidade);

    listarEstados() {
        this.cidadeService.findAllCidadesStateName(this.estadoSelecionada).subscribe(res => {
            this.cidade = res;
        });
    }


    IrParaTelaDeEditar() {
        this.router['account-edit'];
    }
}
