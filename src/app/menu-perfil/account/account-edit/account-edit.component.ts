import {Component, OnInit} from '@angular/core';
import {Address, City, Ordem, State, Usuario} from '../../../interface/ec-template.interface';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../services/data.service';
import {Router} from '@angular/router';
import {CidadeService} from '../../../services/cidade.service';
import {EstadoService} from '../../../services/estado.service';
import {AuthService} from '../../../guard/auth.service';
import {UsuarioService} from '../../../services/usuario.service';
import {AddressService} from '../../../services/address.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-account-edit',
  templateUrl: './account-edit.component.html',
  styleUrls: ['./account-edit.component.scss']
})
export class AccountEditComponent implements OnInit {

  data: Ordem;
  // @ts-ignore
  city: City = {
    id: "",
    name: "",

  }
  state: State = {
    id: "",
    name:"",
    initials:"",

  }
  form: FormGroup;
  cidade: City[] = [];
  estados: State[] = [];
  cidadeSelecionada = '';
  estadoSelecionada = '';

  address: Address = {
    cep: '', city: this.city, complement: '', number: '', road: '',
    id: "",cityName :'',
  };

  usuario: Usuario = {
    sub: '',
    name: '',
    email: '',
    phone: '',
    address: this.address,
    form_payment: undefined
  };


  buttonDisable: boolean = false;


  constructor(private dataService: DataService,
              private fb: FormBuilder,
              private router: Router,
              private cidadeService: CidadeService,
              private estadoService: EstadoService,
              private authService: AuthService,
              private usuarioService: UsuarioService,
              private addressService: AddressService,
              private notifierService: NotifierService,
  ) {
  }

  ngOnInit() {
    this.usuarioService.findUsuarioById(this.authService.getLoggedUser().sub).subscribe(
      respota => {
          this.usuario = respota;
        if(this.usuario.address == null){
          this.usuario.address = this.address;
        }


        this.form = this.fb.group({
          usuarioInfo: this.fb.group({
            name: [this.authService.getLoggedUser().name],
            email: [this.authService.getLoggedUser().email],
            phone: [this.usuario.phone,Validators.required]
          }),
          addressInfo: this.fb.group({
            road: [this.usuario.address.road || '',Validators.required],
            number: [this.usuario.address.number || '',Validators.required],
            complement: [this.usuario.address.complement || '',Validators.required],
            district: [this.usuario.address.district || '',Validators.required],
            cep: [this.usuario.address.cep || '',Validators.required],
          })
        });
        if(this.usuario.address.city.id != ""){
          this.estadoSelecionada = this.usuario.address.city.state.id;
          this.cidadeSelecionada = this.usuario.address.city.id;
          this.listarCidadesByEstados();
        }

      }

    )

    this.estadoService.findAllEstados().subscribe(res => {
      this.estados = res;
    });


  }

  onSubmit() {

    if (this.form.valid) {
    this.buttonDisable = true;
    this.city.id = this.cidadeSelecionada;
    this.state.id = this.estadoSelecionada;
    this.usuario.phone = this.form.get('usuarioInfo').value.phone;

    this.usuario.address = this.form.get('addressInfo').value;
    this.usuario.address.city =  this.city;
    this.usuario.address.city.state =  this.state;


    this.usuarioService.updateUser(this.usuario).subscribe(user => {
      this.buttonDisable = false;
      this.router.navigate(['/menu-perfil/account-information']);
      this.notifierService.notify(
          'success',
          `Dados Editados com Exito`
      );
    }, err => {
      this.buttonDisable = false;
      this.notifierService.notify(
          'info',
          `Verifique os Dados e tente novamente.`
      );
    })
      this.scrollToTop();

    }else {
      this.validateAllFormFields(this.form);
      this.scrollToTop();
    }
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  isRequiredValid(field: string) {
    return this.form.get(field).hasError('required') && this.form.get(field).touched;
  }

  isMinLengthValid(field: string) {
    return this.form.get(field).hasError('minlength') && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }


  scrollToTop() {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 6);
  }

  // mark all field as touched
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  // Control input's limit characters
  limitedInputLength(field: string, limit: number) {
    const formControl = this.form.get(field);
    if (formControl.value.length > limit) {
      formControl.setValue(formControl.value.slice(0, limit));
    }
  }

  controlEstado = new FormControl(this.estadoSelecionada)
  controlCidade = new FormControl(this.controlCidade)

  listarCidadesByEstados() {
    this.cidadeService.findAllCidadesStateName(this.estadoSelecionada).subscribe(res => {
      this.cidade = res;
    });
  }

  IrParaTelaDeEditar() {
    this.router['account-edit'];
  }

  pesquisarCep() {
    this.address = this.form.get('addressInfo').value;

    this.addressService.getEnderecoByCep(this.address.cep).subscribe(resposta => {

      this.address.cep = resposta.cep;
      this.address.complement = resposta.complemento;
      this.address.road = resposta.logradouro;
      this.address.city.name = resposta.localidade;
      this.address.city.state.initials = resposta.uf
      console.log(this.address);
    })
  }
}
