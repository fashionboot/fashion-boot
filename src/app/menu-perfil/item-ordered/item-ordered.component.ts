import { Component, OnInit } from '@angular/core';
import {ItemOrdered, Ordem} from '../../interface/ec-template.interface';
import {OrdemService} from '../../services/ordem.service';
import {AuthService} from '../../guard/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment.prod';

@Component({
  selector: 'app-item-ordered',
  templateUrl: './item-ordered.component.html',
  styleUrls: ['./item-ordered.component.scss']
})
export class ItemOrderedComponent implements OnInit {

  page = 1;
  pageSize = 4;
  collectionSize;
  tamanho;
  itemOrdered: ItemOrdered[] = [];
  pronto: boolean = false;



  constructor(private ordemService: OrdemService, private authService: AuthService, private router: Router,   private route: ActivatedRoute) {
    this.refreshCountries();

  }

  ngOnInit() {
    this.ordemService.findAllItemOrderedByOrdemId(environment.ORDEM_ID!).subscribe(res => {

      this.itemOrdered = res;
      this.collectionSize = this.itemOrdered.length;
      this.refreshCountries();
      this.pronto =true;

    });

  }

  refreshCountries() {
    this.itemOrdered
        .map((country, i) => ({id: i + 1, ...country}))
        .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }
}
