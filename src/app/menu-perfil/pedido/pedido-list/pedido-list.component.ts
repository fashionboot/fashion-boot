import {Component, OnInit} from '@angular/core';
import {OrdemService} from '../../../services/ordem.service';
import {AuthService} from '../../../guard/auth.service';
import {Ordem} from '../../../interface/ec-template.interface';
import {environment} from '../../../../environments/environment.prod';
import {Router} from '@angular/router';

@Component({
  selector: 'app-pedido-list',
  templateUrl: './pedido-list.component.html',
  styleUrls: ['./pedido-list.component.scss']
})
export class PedidoListComponent implements OnInit {


  page = 1;
  pageSize = 4;
  collectionSize;
  tamanho;
  pedidos: Ordem[] = [];
  pronto: boolean = false;




  constructor(private ordemService: OrdemService, private authService: AuthService, private router: Router) {
  this.refreshCountries();

  }

  ngOnInit() {
    this.ordemService.findAllOrdernsByUsuarioSub(this.authService.getLoggedUser().sub).subscribe(res => {
      this.pedidos = res;
      this.collectionSize = this.pedidos.length;
      this.refreshCountries();
      this.pronto =true;

    });

  }

  refreshCountries() {
    this.pedidos
      .map((country, i) => ({id: i + 1, ...country}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

    verDetalhes(id: any) {
      environment.ORDEM_ID = id;
      this.router.navigate(['menu-perfil/pedidos/itens'])
    }
}
