import { Component, OnInit } from '@angular/core';
import {City, Ordem, PaymentInfo, State, Usuario} from '../../../interface/ec-template.interface';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../services/data.service';
import {Router} from '@angular/router';
import {CidadeService} from '../../../services/cidade.service';
import {EstadoService} from '../../../services/estado.service';
import {UsuarioService} from '../../../services/usuario.service';
import {AuthService} from '../../../guard/auth.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-payments-edit',
  templateUrl: './payments-edit.component.html',
  styleUrls: ['./payments-edit.component.scss']
})
export class PaymentsEditComponent implements OnInit {


  form: FormGroup;
  buttonDisable: boolean = false;

  pagamento: PaymentInfo = {
    cvv: null, holderName: '', id: '', number: null, validate: '',

  };
  usuario: Usuario = {
    address: undefined, email: '', form_payment: this.pagamento, name: '', phone: '', sub: ''

  };


  constructor(private dataService: DataService,
              private fb: FormBuilder,
              private router: Router,
              private usuarioService: UsuarioService,
              private authService: AuthService,
              private notifierService: NotifierService,) {
  }

  ngOnInit() {
    this.usuarioService.findUsuarioById(this.authService.getLoggedUser().sub).subscribe(resposta => {
      this.usuario = resposta;
      if(this.usuario.form_payment == null){
        this.usuario.form_payment = this.pagamento;
      }
      console.log(this.usuario)
      this.form = this.fb.group({
        paymentInfo: this.fb.group({
          holderName: [this.usuario.form_payment.holderName,Validators.required],
          number: [this.usuario.form_payment.number,Validators.required],
          validate: [this.usuario.form_payment.validate,Validators.required],
          cvv: ['']
        })
      });
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.buttonDisable = true;
    this.usuario.form_payment = this.form.get('paymentInfo').value;
    this.usuarioService.updateUser(this.usuario).subscribe(user => {
      this.buttonDisable = false;
      this.notifierService.notify(
        'success',
        `Pagamento Editado com Exito`
      );
      this.router.navigate(['/menu-perfil/payments']);
    }, error => {
      this.buttonDisable = false;
      this.notifierService.notify(
          'info',
          `Verifique os dados e tente novamente.`
      );

    })
  }else {
      this.validateAllFormFields(this.form);
      this.scrollToTop();
    }
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  isRequiredValid(field: string) {
    return this.form.get(field).hasError('required') && this.form.get(field).touched;
  }

  isMinLengthValid(field: string) {
    return this.form.get(field).hasError('minlength') && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  // mark all field as touched
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  // Set delivery info if check box is checked
  toggleDeliveryCB(isChecked: boolean) {
    const recipientName = this.form.get('addressInfo.road');
    const recipientNumber = this.form.get('addressInfo.number');
    if (isChecked) {
      recipientName.setValue(this.form.get('usuarioInfo.name').value);
      recipientNumber.setValue(this.form.get('usuarioInfo.number').value);
    } else {
      recipientName.reset();
      recipientNumber.reset();
    }
  }

  // Control input's limit characters

  limitedInputLength(field: string, limit: number) {
    const formControl = this.form.get(field);
    if (formControl.value.length > limit) {
      formControl.setValue(formControl.value.slice(0, limit));
    }
  }

  checkExpiredDatePattern(inputEvent: any) {
    const formControl = this.form.get('paymentInfo.validate');
    if (inputEvent.inputType === 'insertText') {
      if (formControl.value.length > 5) {
        // can't be more than 5 characters
        formControl.setValue(formControl.value.slice(0, 5));
      } else if (formControl.value.length === 3) {
        // add '/' after the second number
        const front = formControl.value.slice(0, 2);
        const back = formControl.value.slice(2, 4);
        formControl.setValue(`${front}/${back}`);
      }
    }
  }



  scrollToTop() {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 6);
  }


}
