import {Component, OnInit} from '@angular/core';
import {City, Ordem, PaymentInfo, State, Usuario} from '../../../interface/ec-template.interface';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../../services/data.service';
import {Router} from '@angular/router';
import {CidadeService} from '../../../services/cidade.service';
import {EstadoService} from '../../../services/estado.service';
import {UsuarioService} from '../../../services/usuario.service';
import {AuthService} from '../../../guard/auth.service';

@Component({
    selector: 'app-payments-information',
    templateUrl: './payments-information.component.html',
    styleUrls: ['./payments-information.component.scss']
})
export class PaymentsInformationComponent implements OnInit {

    data: Ordem;
    // @ts-ignore
    city: City = {
        id: '',
    };
    form: FormGroup;

    pagamento: PaymentInfo = {
        cvv: null, holderName: '', id: '', number: null, validate: ''

    };
    usuario: Usuario = {
        address: undefined, email: '', form_payment: this.pagamento, name: '', phone: '', sub: ''

    };

    constructor(private dataService: DataService,
                private fb: FormBuilder,
                private router: Router,
                private usuarioService: UsuarioService,
                private authService: AuthService
    ) {
    }

    ngOnInit() {
        this.usuarioService.findUsuarioById(this.authService.getLoggedUser().sub).subscribe(resposta => {
            this.usuario = resposta;
            if(this.usuario.form_payment == null){
                this.usuario.form_payment = this.pagamento;
            }
            this.form = this.fb.group({
                paymentInfo: this.fb.group({
                    holderName: [this.usuario.form_payment.holderName],
                    number: [this.usuario.form_payment.number],
                    validade: [this.usuario.form_payment.validate],
                    cvv: ['***']
                })
            });
        });
    }



    scrollToTop() {
        const scrollToTop = window.setInterval(() => {
            const pos = window.pageYOffset;
            if (pos > 0) {
                window.scrollTo(0, pos - 20); // how far to scroll on each step
            } else {
                window.clearInterval(scrollToTop);
            }
        }, 6);
    }


}
