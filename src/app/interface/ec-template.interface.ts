import {DropdownItem} from './universal.interface';

export interface CarouselInfo {
  title: string;
  description: string;
  img: string;
  button: {
    display: boolean;
    content?: string;
    href?: string;
  };
}

export interface GalleryImages {
  id: string;
  name: string;
}

export interface Options {
  id: string;
  value: string;
  name: string;
}

export interface Product {
  id: string;
  name: string;
  description: string;
  detail?: string;
  category: CategoryInfo;
  img: string;
  gallery?: GalleryImages[];
  onSale: boolean;
  salePrice?: string;
  options?: Options[];
  inStock: boolean;
}

export interface CategoryInfo {
  id: string;
  name: string;
  redirect: string;
  count?: number;
  products?: Product[];
}

export interface BrandInfo {
  id: string;
  name: string;
  redirect: string;
  count?: number;
  products?: Product[];
}


export interface ItemOrdered {
  id?: string;
  product: Product;
  quantity: number;
  option: Options;
}

export interface Ordem {
  id?: string;
  itemOrdered?: ItemOrdered[];
  totalPrice?: number;
  usuario?: Usuario;
  status?: string;
  date_purchase: string;
}

export interface Usuario {
  sub: string;
  name: string;
  email: string;
  phone: string;
  form_payment:PaymentInfo;
  address:Address;
}

export interface PaymentInfo {

  id?: string;
  holderName?: string;
  number?: number;
  validate?: string;
  cvv?: number;
}

export interface Address {
  id: string;
  road: string;
  number: string;
  complement: string;
  district?: string;
  city: City;
  cep: string;
  cityName:string;

}

export interface City{
  id: string;
  name: string;
  state: State;
}
export interface State{
  id: string;
  name: string;
  initials: string;
}
