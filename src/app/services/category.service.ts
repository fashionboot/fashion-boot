import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CategoryInfo, Product} from '../interface/ec-template.interface';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {


  baseUrl: String = environment.baseUrl;

  constructor(private http: HttpClient) {

  }
  //Pegando todas as Categorias
  findAllCategorias(): Observable<CategoryInfo[]> {
    const url = `${this.baseUrl}/categorias`;
    return this.http.get<CategoryInfo[]>(url);
  }

}
