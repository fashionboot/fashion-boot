import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Observable} from 'rxjs';
import {BrandInfo, CategoryInfo} from '../interface/ec-template.interface';

@Injectable({
  providedIn: 'root'
})
export class BrandService {

  baseUrl: String = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  //metodo para trazer todas as MARCAS
  findAllBrands(): Observable<BrandInfo[]> {
    const url = `${this.baseUrl}/brands`;
    return this.http.get<BrandInfo[]>(url);

  }
}
