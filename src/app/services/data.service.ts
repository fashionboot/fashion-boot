import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {
  Product,
  CategoryInfo,
  ItemOrdered,
  Ordem, PaymentInfo, Address, Usuario, State, City
} from '../interface/ec-template.interface';
import {BehaviorSubject, forkJoin, Observable} from 'rxjs';
import {NotifierService} from 'angular-notifier';
import {Router} from '@angular/router';
import {CategoryService} from './category.service';
import {ProductService} from './product.service';
import {environment} from '../../environments/environment';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AuthService} from '../guard/auth.service';

const SHOPPING_CART_KEY = 'shopping-cart-date_purchase';
const ORDER_INFO_KEY = 'order-info';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private headers = new HttpHeaders();
  productList$ = new BehaviorSubject<Product[]>([]);
  categoryList$ = new BehaviorSubject<CategoryInfo[]>([]);
  currentCategory$ = new BehaviorSubject<string>('all');
  currentProductListByCategory$ = new BehaviorSubject<Product[]>([]);

  baseUrl: String = environment.baseUrl;
  shoppingCartData: ItemOrdered[] = [];

  constructor(
      private   auth: AuthService,
    private http: HttpClient,
    private notifierService: NotifierService,
    private router: Router,
    private cateService: CategoryService,
    private productyService: ProductService,
  ) {
    this.initData();
    this.currentCategory$.subscribe(() => {
      if (this.categoryList$.value.length !== 0) {
        this.getProductListByCategory();
      }
    });
  }

  setHeaders(key: string, value: string) {
    this.headers.set(key, value);
  }



  private initData() {
    this.loadShoppingCart();
    forkJoin(this.getAllProductList(), this.getCategoryList()).subscribe((data: any) => {
      this.productList$.next(data[0]);
      console.log('products:', this.productList$.value);
      this.categoryList$.next(data[1]);
      console.log('categories:', this.categoryList$.value);
      this.setCategoryCount();
      this.getProductListByCategory();
    });
  }

  private getAllProductList() {
    return this.productyService.findAllProducts();
  }

  private getCategoryList() {
    return this.cateService.findAllCategorias();
  }

  private setLocalStorage(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  private getLocalStorage(key: string) {
    return JSON.parse(localStorage.getItem(key));
  }

  private removeLocalStorage(key: string) {
    localStorage.removeItem(key);
  }
  setCurrentCategory(category: string) {
    this.currentCategory$.next(category);
  }

  private setCategoryCount() {
    // reset
    for (const category of this.categoryList$.value) {
      category.count = 0;
      category.products = [];
    }
    for (const product of this.productList$.value) {
      for (const c of this.categoryList$.value) {
        if (product.category.name === c.name) {
          c.count++;
          c.products.push(product);
        }
      }
    }
  }

  private getProductListByCategory() {
    return this.productyService.getProductListByCategory("Roupas");
  }

  getRelatedProductsByCategory(productId: string, category: string, amount: number) {
    const relatedProducts: Product[] = [];


    for (const c of this.categoryList$.value) {
      if (c.name === category) {
        for (let i = 0; relatedProducts.length < 3; i++) {
          if (c.products[i] && c.products[i].id !== productId) {
            relatedProducts.push(c.products[i]);
          }
        }
      }
    }
    return relatedProducts;
  }


  private loadShoppingCart() {
    if (this.getLocalStorage(SHOPPING_CART_KEY)) {
      this.shoppingCartData = this.getLocalStorage(SHOPPING_CART_KEY);
    }
    console.log('SC Data from LocalStorage', this.shoppingCartData);
  }

  addShoppingCartItem(item: ItemOrdered) {
    if (
      this.shoppingCartData.find(data => {
        return data.product.id === item.product.id && data.option.value === item.option.value;
      })
    ) {
      for (const i of this.shoppingCartData) {
        if (i.product.id === item.product.id && i.option.value === item.option.value) {
          i.quantity = i.quantity + item.quantity;
        }
      }
    } else {
      this.shoppingCartData = [...this.shoppingCartData, item];
    }
    console.log('item adicionado:', this.shoppingCartData);
    this.setLocalStorage(SHOPPING_CART_KEY, this.shoppingCartData);
    this.notifierService.notify(
      'default',
      `Adicionado ${item.product.name} - ${item.option.name.toUpperCase()} ao carrinho`
    );
  }

  editShoppingCartItem(item: ItemOrdered) {
    this.shoppingCartData = this.shoppingCartData.map((data: ItemOrdered) => {
      if (data.product.id === item.product.id) {
        data = Object.assign({}, data, item);
      }
      return data;
    });
    console.log('item edited:', this.shoppingCartData);
    this.setLocalStorage(SHOPPING_CART_KEY, this.shoppingCartData);
  }

  deleteShoppingCartItem(item: ItemOrdered) {
    this.shoppingCartData = this.shoppingCartData.filter(
      data => !(data.product.id === item.product.id && data.option.value === item.option.value)
    );
    console.log('item removido:', this.shoppingCartData);
    this.setLocalStorage(SHOPPING_CART_KEY, this.shoppingCartData);
    this.notifierService.notify(
      'warning',
      `Removido ${item.product.name} - ${item.option.name.toUpperCase()}`
    );
  }

  saveOrderInfo(data: Ordem) {
    this.setLocalStorage(ORDER_INFO_KEY, data);
  }

  getOrderInfo() {
    return this.getLocalStorage(ORDER_INFO_KEY);
  }

  submitOrder(order: Ordem) {

    let options: HttpParams = new HttpParams();
    let headers = new HttpHeaders(

    );
    const formData = new FormData();
    // formData.append('formPayment', new Blob([JSON.stringify(order.usuario.form_payment)], {type: 'application/json'}));
    // formData.append('address', new Blob([JSON.stringify(order.usuario.address)], {type: 'application/json'}));
    // formData.append('usuario', new Blob([JSON.stringify(order.usuario)], {type: 'application/json'}));
     formData.append('itemOrdered', new Blob([JSON.stringify(order.itemOrdered)], {type: 'application/json'}));
    formData.append('ordem', new Blob([JSON.stringify(order)], {type: 'application/json'}));

    this.http.post(`${this.baseUrl}/orders/compra`, formData, {
      headers: headers,
      params: options,
      observe: 'response'
    }).subscribe(resposta => {
      this.notifierService.notify('success', 'Compra feita Sucesso');
        this.removeLocalStorage(SHOPPING_CART_KEY);
        this.removeLocalStorage(ORDER_INFO_KEY);
        this.shoppingCartData = [];
        setTimeout(() => {
          this.router.navigate(['menu-perfil/pedidos']);
        }, 1000);
      }
    );

  }


}
