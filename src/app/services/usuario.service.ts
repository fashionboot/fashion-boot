import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Usuario} from '../interface/ec-template.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {


  baseUrl: String = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  createUser(user: { name: string; sub: string; email: string }): Observable<Usuario> {
    const url = `${this.baseUrl}/usuarios`;
    return this.http.post<Usuario>(url, user);
  }

  updateUser(user: Usuario): Observable<Usuario> {
    const url = `${this.baseUrl}/usuarios`;
    return this.http.put<Usuario>(url, user);
  }


  findallUsers(): Observable<Usuario[]> {
    const url = `${this.baseUrl}/usuarios`;
    return this.http.get<Usuario[]>(url);
  }

  findUsuarioById(id: String): Observable<Usuario> {
    const url = `${this.baseUrl}/usuarios/${id}`;
    return this.http.get<Usuario>(url);
  }


}
