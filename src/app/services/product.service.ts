import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {CategoryInfo, Product} from '../interface/ec-template.interface';


@Injectable({
  providedIn: 'root'
})
export class ProductService {


  baseUrl: String = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  findAllProducts(): Observable<Product[]> {
    const url = `${this.baseUrl}/products`;
    return this.http.get<Product[]>(url);
  }

  findProductById(id: String): Observable<Product> {
    const url = `${this.baseUrl}/products/${id}`;
    return this.http.get<Product>(url);
  }

  getProductListByCategory(id: string): Observable<Product[]> {
    const url = `${this.baseUrl}/products/categoria/${id}`;
    return this.http.get<Product[]>(url);
  }

  getProductListByMarca(name: string): Observable<Product[]> {
    const url = `${this.baseUrl}/products/marca/${name}`;
    return this.http.get<Product[]>(url);
  }

  findProductsByName(name: string): Observable<Product[]> {
  const url = `${this.baseUrl}/products/produto/${name}`;
  return this.http.get<Product[]>(url);
}

}
