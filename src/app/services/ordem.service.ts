import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ItemOrdered, Ordem, Product} from '../interface/ec-template.interface';

@Injectable({
  providedIn: 'root'
})
export class OrdemService {

  baseUrl: String = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  findAllItemOrderedByOrdemId(id: any): Observable<ItemOrdered[]> {
    const url = `${this.baseUrl}/item_ordered/orders/${id}`;
    return this.http.get<ItemOrdered[]>(url);

  }


  findAllOrdernsByUsuarioSub(sub: string): Observable<Ordem[]> {
    const url = `${this.baseUrl}/orders/usuario/${sub}`;
    return this.http.get<Ordem[]>(url);
  }


}
