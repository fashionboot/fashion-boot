import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Address, City, State, Usuario} from '../interface/ec-template.interface';
import {FormGroup} from '@angular/forms';
import {NotifierService} from 'angular-notifier';

@Injectable({
    providedIn: 'root'
})
export class AddressService {

    baseUrl: String = environment.baseUrl;

    constructor(private http: HttpClient, private notifierService: NotifierService) {
    }

    createAddress(address: Address): Observable<Address> {
        const url = `${this.baseUrl}/address`;
        return this.http.post<Address>(url, address);
    }

    getEnderecoByCep(cep: String): Observable<cepObject> {

        const url = `viacep.com.br/ws/${cep}/json/`;
        return this.http.get<cepObject>(url);
    }


}

export interface cepObject {
    cep: string;
    logradouro: string;
    complemento: string;
    bairro: string;
    localidade: string;
    uf: string;
    ibge: string;
    gia: string;
    ddd: string;


}
