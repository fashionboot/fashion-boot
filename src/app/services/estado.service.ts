import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {City, State} from "../interface/ec-template.interface";

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  baseUrl: String = environment.baseUrl;

  constructor(private http: HttpClient) {

  }

  findAllEstados(): Observable<State[]> {
    const url = `${this.baseUrl}/states`;
    return this.http.get<State[]>(url);
  }
}
