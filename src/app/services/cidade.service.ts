import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CategoryInfo, City} from "../interface/ec-template.interface";

@Injectable({
  providedIn: 'root'
})
export class CidadeService {

  baseUrl: String = environment.baseUrl;

  constructor(private http: HttpClient) {

  }
  //Pegando todas as Categorias
  findAllCidades(): Observable<City[]> {
    const url = `${this.baseUrl}/citys`;
    return this.http.get<City[]>(url);
  }
  findAllCidadesStateName(name:string): Observable<City[]> {
    const url = `${this.baseUrl}/citys/state/${name}`;
    return this.http.get<City[]>(url);
  }
}
