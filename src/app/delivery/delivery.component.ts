import { Component, OnInit } from '@angular/core';
import {City, Ordem, State} from '../interface/ec-template.interface';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../services/data.service';
import {Router} from '@angular/router';
import {CidadeService} from '../services/cidade.service';
import {EstadoService} from '../services/estado.service';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit {
  data: Ordem;
  // @ts-ignore
  city: City = {
    id: "",
  };
  form: FormGroup;
  cidade: City[] = [];
  estados: State[] = [];
  cidadeSelecionada = '';
  estadoSelecionada = 'Default';



  constructor(private dataService: DataService,
              private fb: FormBuilder,
              private router: Router,
              private cidadeService: CidadeService,
              private estadoService: EstadoService) {
  }

  ngOnInit() {

    this.estadoService.findAllEstados().subscribe(res => {
      this.estados = res;
    })
    this.form = this.fb.group({
      addressInfo: this.fb.group({
        road: ['rua macaco', Validators.required],
        number: ['321', Validators.required],
        complement: ['casa 2', Validators.required],
        district: ['novo mundo'],
        cep: ['32659842', Validators.required],
        country: ['brasil', Validators.required]
      })
    });
  }

  onSubmit() {
    this.city.id = this.cidadeSelecionada;
    if (this.form) {
      this.data.usuario.address = this.form.get('addressInfo').value;
      this.data.usuario.address.city = this.city;
      //  this.date_purchase.usuario.address.city.state.id =  this.estadoSelecionada;
      this.dataService.submitOrder(this.data);
    } else {
      this.validateAllFormFields(this.form);
      this.scrollToTop();
    }
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  isRequiredValid(field: string) {
    return this.form.get(field).hasError('required') && this.form.get(field).touched;
  }

  isMinLengthValid(field: string) {
    return this.form.get(field).hasError('minlength') && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  // mark all field as touched
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  // Set delivery info if check box is checked
  toggleDeliveryCB(isChecked: boolean) {
    const recipientName = this.form.get('addressInfo.road');
    const recipientNumber = this.form.get('addressInfo.number');
    if (isChecked) {
      recipientName.setValue(this.form.get('usuarioInfo.password').value);
      recipientNumber.setValue(this.form.get('usuarioInfo.number').value);
    } else {
      recipientName.reset();
      recipientNumber.reset();
    }
  }

  // Control input's limit characters
  limitedInputLength(field: string, limit: number) {
    const formControl = this.form.get(field);
    if (formControl.value.length > limit) {
      formControl.setValue(formControl.value.slice(0, limit));
    }
  }

  checkExpiredDatePattern(inputEvent: any) {
    const formControl = this.form.get('paymentInfo.validate');
    if (inputEvent.inputType === 'insertText') {
      if (formControl.value.length > 5) {
        // can't be more than 5 characters
        formControl.setValue(formControl.value.slice(0, 5));
      } else if (formControl.value.length === 3) {
        // add '/' after the second number
        const front = formControl.value.slice(0, 2);
        const back = formControl.value.slice(2, 4);
        formControl.setValue(`${front}/${back}`);
      }
    }
  }

  getTotalItems() {
    let total = 0;
    for (const i of this.data.itemOrdered) {
      total += +i.quantity;
    }
    return total;
  }

  scrollToTop() {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 6);
  }


  controlCidade = new FormControl("")
  controlEstado = new FormControl("")

  findCidades() {
    return this.cidadeService.findAllCidadesStateName(this.estadoSelecionada).subscribe(res => {
      this.cidade = res;
    })
  }
}
