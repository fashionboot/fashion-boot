import {Component, OnInit} from '@angular/core';

import { CarouselInfo } from '../../interface/ec-template.interface';



@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  data: CarouselInfo[] = [
    {
      title: '',
      description: '',
      img: 'assets/images/carrocel/Fashion-image1.png',
      button: {display: false}
    },
    {
      title: '',
      description: '',
      img: 'assets/images/carrocel/Fashion-image2.png',
      button: {display: false}
    },
    {
      title: '',
      description: '',
      img: '/assets/images/carrocel/Fashion-image3.png',
      button: {display: false}
    }
  ];

  constructor() {
  }

  ngOnInit() {
  }
}
