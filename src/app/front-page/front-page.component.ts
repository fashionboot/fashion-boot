import {Component, OnInit} from '@angular/core';
import {DataService} from '../services/data.service';
import {Product, Usuario} from '../interface/ec-template.interface';
import {ProductService} from '../services/product.service';
import {AuthService} from '../guard/auth.service';
import { UsuarioService } from '../services/usuario.service';


@Component({
    selector: 'app-front-page',
    templateUrl: './front-page.component.html',
    styleUrls: ['./front-page.component.scss']
})
export class FrontPageComponent implements OnInit {
    productList: Product[];
    user: { name: string; sub: string; email: string } = {
        sub: '',
        email: '',
        name: '',

    };

    constructor(private dataService: ProductService, private authService: AuthService, private usuarioService: UsuarioService) {
    }

    ngOnInit() {
       this.verificarUsuario();
        this.dataService.findAllProducts().subscribe(data => {
            this.productList = data;
        });
    }

    verificarUsuario(){
        try {
            let loggedUser = this.authService.getLoggedUser();

            let teste = sessionStorage.getItem('crioconta')
            if(loggedUser != null && teste == null){
                this.user.sub = loggedUser.sub;
                this.user.name = loggedUser.name;
                this.user.email = loggedUser.email;
                this.usuarioService.createUser(this.user).subscribe(r => {
                    if(r == null){
                        sessionStorage.setItem('crioconta', 'true')
                        sessionStorage.setItem('idUsuario', loggedUser.sub)
                    }

                });
            }
        }catch (e){

        }

    }
}
