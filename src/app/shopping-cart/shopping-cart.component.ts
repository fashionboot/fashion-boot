import {Component, OnInit} from '@angular/core';
import {DataService} from '../services/data.service';
import {ItemOrdered, Ordem} from '../interface/ec-template.interface';
import {AuthService} from '../guard/auth.service';
import {UsuarioService} from '../services/usuario.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
  data: ItemOrdered[] = [];
  // order summary
  subTotal = 0;
  shippingFee = 50;
  taxPercentage = 5; // 5%
  tax = 0;
  total = 0;
  user: { name: string; sub: string; email: string } = {
    sub: '',
    email: '',
    name: '',

  };


  constructor(private dataService: DataService, private authService: AuthService, private usuarioService: UsuarioService) {
  }

  ngOnInit() {
    this.verificarUsuario();
    this.data = this.dataService.shoppingCartData;
    this.getOrderSummary();
  }

  updateItem(item: ItemOrdered) {
    this.dataService.editShoppingCartItem(item);
    this.getOrderSummary();
  }

  removeItem(item: ItemOrdered) {
    this.dataService.deleteShoppingCartItem(item);
    this.data = this.dataService.shoppingCartData;
    this.getOrderSummary();
  }

  getTotalPrice(item: ItemOrdered) {
    if (item.product.onSale) {
      return item.quantity * +item.product.salePrice;
    }
  }

  getOrderSummary() {
    this.subTotal = 0;
    for (const i of this.data) {
      if (i.product.onSale) {
        this.subTotal = this.subTotal + +i.product.salePrice * i.quantity;
      }
    }
    this.tax = (this.subTotal * this.taxPercentage) / 100;
    this.total = this.subTotal + this.shippingFee + this.tax;
  }

  onCheckOut() {
    this.dataService.saveOrderInfo(<Ordem>{
      itemOrdered: this.data,
      totalPrice: this.total
    });
  }

  verificarUsuario(){
    try {
      let loggedUser = this.authService.getLoggedUser();
      let teste = sessionStorage.getItem('crioconta')
      if(loggedUser != null && teste == null){
        this.user.sub = loggedUser.sub;
        this.user.name = loggedUser.name;
        this.user.email = loggedUser.email;
        this.usuarioService.createUser(this.user).subscribe(r => {
          if(r == null){
            sessionStorage.setItem('crioconta', 'true')
            sessionStorage.setItem('idUsuario', loggedUser.sub)
          }

        });
      }
    }catch (e){

    }
}
}
