import {Component, OnInit, ViewChild} from '@angular/core';
import {DataService} from '../services/data.service';
import {Address, City, Ordem, PaymentInfo, State, Usuario} from '../interface/ec-template.interface';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {CidadeService} from '../services/cidade.service';
import {EstadoService} from '../services/estado.service';
import {UsuarioService} from '../services/usuario.service';
import {AuthService} from '../guard/auth.service';
import {NotifierService} from 'angular-notifier';

@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  data: Ordem;
  // @ts-ignore
  city: City = {
    id: '',
  };
  form: FormGroup;
  cidade: City[] = [];
  estados: State[] = [];
  cidadeSelecionada = '';
  estadoSelecionada = 'Default';

  load:boolean=true;
  rebuy:boolean=false;

  form_payment: PaymentInfo = {
    id: '',
    holderName: '',
    number: null,
    cvv: null,
    validate: '',

  }



  address: Address = {
    cep: '', city: this.city, complement: '', number: '', road: '',
    id: '',cityName: 'a',
  };
  usuario: Usuario = {
    sub: '',
    name: '',
    email: '',
    phone: '',
    address: this.address,
    form_payment: this.form_payment
  };


  constructor(private dataService: DataService,
              private fb: FormBuilder,
              private router: Router,
              private cidadeService: CidadeService,
              private estadoService: EstadoService,
              private usuarioService: UsuarioService,
              private authService: AuthService,
              private notifierService: NotifierService,) {
  }

  ngOnInit() {

    this.usuarioService.findUsuarioById(this.authService.getLoggedUser().sub).subscribe(
      respota => {
        console.log(respota)
        this.usuario = respota;
        this.usuario.address.cityName = "a";
        if(this.usuario.address == null){
            this.usuario.address = this.address
        }
        if(this.usuario.form_payment == null){
            this.usuario.form_payment = this.form_payment
        }
        if (this.checkObject(this.usuario.address)) {
          this.notifierService.notify(
            'warning',
            `Complete seu perfil, está faltando endereço`
          );
          this.router.navigate(['/menu-perfil/account-edit']);
        }
        console.log(this.usuario.form_payment)

        if (this.checkObjecte(this.usuario.form_payment)) {

          this.notifierService.notify(
            'warning',
            `Complete seu perfil, está faltando forma de pagamento`
          );
          this.router.navigate(['/menu-perfil/payments-edit']);
        }
        this.estadoService.findAllEstados().subscribe(res => {
          this.estados = res;
        });
        this.scrollToTop();

        this.data = this.dataService.getOrderInfo();
        // redirect to shopping cart page if no item in the cart
        if (this.data.itemOrdered.length === 0) {
          this.router.navigate(['shopping-cart']);
        }

        this.form = this.fb.group({
          usuarioInfo: this.fb.group({
            name: [{value: this.usuario.name, disabled: true}],
            email: [{value: this.usuario.email, disabled: true}],
            number: [{value: this.usuario.phone, disabled: true}],
          }),
          paymentInfo: this.fb.group({
            holderName: [{value: this.usuario.form_payment.holderName, disabled: true}],
            number: [{value: this.usuario.form_payment.number, disabled: true}],
            validate: [{value: this.usuario.form_payment.validate, disabled: true}],
            cvv: [{value: this.usuario.form_payment.cvv, disabled: true}],
          }),
          addressInfo: this.fb.group({
            road: [{value: this.usuario.address.road, disabled: true}],
            number: [{value: this.usuario.address.number, disabled: true}],
            complement: [{value: this.usuario.address.complement, disabled: true}],
            district: [{value: this.usuario.address.district, disabled: true}],
            cep: [{value: this.usuario.address.cep, disabled: true}],
          })

        });

        if (this.usuario.address.city != null) {
          this.estadoSelecionada = this.usuario.address.city.state.id;
          this.cidadeSelecionada = this.usuario.address.city.id;
          this.listarEstados();
        }
      });
  }

  onSubmit() {
    if (this.form ) {
      this.scrollToTop();
      this.load=false;
      this.rebuy=true;



      this.data.usuario = this.usuario;
      // this.date_purchase.usuario.address = this.form.get('addressInfo').value;
      // this.date_purchase.usuario.form_payment = this.form.get('paymentInfo').value;
      // this.date_purchase.usuario.address.city = this.city;
      //  this.date_purchase.usuario.address.city.state.sub =  this.estadoSelecionada;
      this.dataService.submitOrder(this.data);

    } else {
      this.validateAllFormFields(this.form);
      this.scrollToTop();
    }
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  isRequiredValid(field: string) {
    return this.form.get(field).hasError('required') && this.form.get(field).touched;
  }

  isMinLengthValid(field: string) {
    return this.form.get(field).hasError('minlength') && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  // mark all field as touched
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  // Set delivery info if check box is checked
  toggleDeliveryCB(isChecked: boolean) {
    const recipientName = this.form.get('addressInfo.road');
    const recipientNumber = this.form.get('addressInfo.number');
    if (isChecked) {
      recipientName.setValue(this.form.get('usuarioInfo.name').value);
      recipientNumber.setValue(this.form.get('usuarioInfo.number').value);
    } else {
      recipientName.reset();
      recipientNumber.reset();
    }
  }

  // Control input's limit characters
  limitedInputLength(field: string, limit: number) {
    const formControl = this.form.get(field);
    if (formControl.value.length > limit) {
      formControl.setValue(formControl.value.slice(0, limit));
    }
  }

  checkExpiredDatePattern(inputEvent: any) {
    const formControl = this.form.get('paymentInfo.validate');
    if (inputEvent.inputType === 'insertText') {
      if (formControl.value.length > 5) {
        // can't be more than 5 characters
        formControl.setValue(formControl.value.slice(0, 5));
      } else if (formControl.value.length === 3) {
        // add '/' after the second number
        const front = formControl.value.slice(0, 2);
        const back = formControl.value.slice(2, 4);
        formControl.setValue(`${front}/${back}`);
      }
    }
  }

  getTotalItems() {
    let total = 0;
    for (const i of this.data.itemOrdered) {
      total += +i.quantity;
    }
    return total;
  }

  scrollToTop() {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 6);
  }

  teste() {
    console.log(this.cidadeSelecionada);
  }

  controlCidade = new FormControl('');
  controlEstado = new FormControl('');

  listarEstados() {
    this.cidadeService.findAllCidadesStateName(this.estadoSelecionada).subscribe(res => {
      this.cidade = res;
    });
  }

  checkObject(obj) {
    let arr = [];
    for (let key in obj) {
      arr.push(obj[key] !== undefined && obj[key] !== null && obj[key] !== "");
    }
    return arr.includes(false);
  }  checkObjecte(obj) {
    let arr = [];
    for (let key in obj) {
      arr.push(obj[key] !== undefined && obj[key] !== null && obj[key] !== "");
    }
    return arr.includes(false);
  }
}
