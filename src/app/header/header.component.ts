import {Component, OnInit} from '@angular/core';
import {DataService} from '../services/data.service';
import {BrandInfo, CategoryInfo} from '../interface/ec-template.interface';
import {BrandService} from '../services/brand.service';
import {CategoryService} from '../services/category.service';
import {AuthService} from '../guard/auth.service';
import {Router} from '@angular/router';
import {FormControl} from '@angular/forms';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    menuCategory: CategoryInfo[] = [];
    menuBrand: BrandInfo[] = [];
    produtoName: string;
    produtoNameControl = new FormControl("")

    bntLogin: boolean = true;
    bntLogout: boolean = true;

    constructor(public dataService: DataService, private service: BrandService,
                private serviceCat: CategoryService, private authService: AuthService,
                private router: Router,) {
    }

    ngOnInit() {
        this.getMenuCategory();
        this.getMenuBrand();
        if (this.authService.getLoggedUser() == null) { //ser for null login fica ativado
            this.bntLogin = true;
            this.bntLogout = false;

        } else {

            this.bntLogout = true;
            this.bntLogin = false;
        }


    }
    getMenuCategory() {
        this.serviceCat.findAllCategorias().subscribe(r => {
            this.menuCategory = r;
        });
    }

    getMenuBrand() {
        this.service.findAllBrands().subscribe(r => {
            this.menuBrand = r;
        });


    }

    logout() {
        // this.bntLogin = true;
        // this.bntLogout = false;
        sessionStorage.removeItem('idUsuario');
        sessionStorage.removeItem('crioconta');
        this.authService.logout();
        // this.bntLogin = true;
        // this.bntLogout = false;


    }

    login() {
        // this.bntLogout = true;
        // this.bntLogin = false;
        this.authService.login();

    }

    listCategoria(name: string) {
        console.log(name);

        this.router.navigate(['/category/'+ name]);
    }

    listByMarca(name: string) {
        this.router.navigate(['category/marca/'+ name]);
    }

    searchProduto(produtoName: string) {
        this.router.navigate(['category/produto/'+ produtoName]);

    }
}
