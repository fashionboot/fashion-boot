import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
// Modules
import {SharedModule} from './shared/shared.module';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {CategoryModule} from './category/category.module';
import {NotifierModule} from 'angular-notifier';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
// Services
import {DataService} from './services/data.service';
// Components
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {FrontPageComponent} from './front-page/front-page.component';
import {CarouselComponent} from './front-page/carousel/carousel.component';
import {ShoppingCartComponent} from './shopping-cart/shopping-cart.component';
import {EmptyComponent} from './empty/empty.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {PaymentComponent} from './payment/payment.component';

import {MatSnackBarModule} from '@angular/material/snack-bar';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {DeliveryComponent} from './delivery/delivery.component';
import {MenuPerfilModule} from './menu-perfil/menu-perfil.module';
import {AppRoutingModule} from './app-routing.module';
import {initializer} from './init/app-init';
import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';
import {AuthGuard} from './guard/auth-guard.service';
import { MarcaListComponent } from './category/marca-list/marca-list.component';
import localePt from '@angular/common/locales/pt';
import {registerLocaleData} from '@angular/common';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
registerLocaleData(localePt);


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        FrontPageComponent,
        CarouselComponent,
        ShoppingCartComponent,
        EmptyComponent,
        NotFoundComponent,
        PaymentComponent,
        LoginComponent,
        RegisterComponent,
        DeliveryComponent,
        MarcaListComponent,
    ],
    imports: [
        MatButtonModule,
        MatIconModule,
      NgbPaginationModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        SharedModule,
        MenuPerfilModule,
        TooltipModule.forRoot(),
        CategoryModule,
        NotifierModule.withConfig({
            position: {
                horizontal: {
                    position: 'right'
                },
                vertical: {
                    position: 'top'
                }
            }
        }),
        KeycloakAngularModule,

    ],
    providers: [
        DataService,
        AuthGuard,
        {
            provide: APP_INITIALIZER,
            useFactory: initializer,
            deps: [KeycloakService],
            multi: true,

        },
        { provide: LOCALE_ID, useValue: "pt-BR" }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
