import {Component, OnInit} from '@angular/core';
import {NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation} from 'ngx-gallery';
import {Options, Product} from '../../interface/ec-template.interface';
import {DataService} from '../../services/data.service';
import {ActivatedRoute} from '@angular/router';
import {DropdownItem} from '../../interface/universal.interface';
import {ProductService} from '../../services/product.service';


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  data: Product;
  quantity = 1;
  option = <Options>{};
  relatedProducts: Product[] = [];
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];

  id: string;

  constructor(private dataService: DataService, private route: ActivatedRoute, private service: ProductService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.service.findProductById(params['id']).subscribe(data => {

        this.data = data;
        this.option = this.data.options[0];
        this.getRelatedProducts();
        this.scrollToTop();
        this.galleryImages = [];
        // insert main image
        this.galleryImages.push({small: this.data.img, medium: this.data.img, big: this.data.img});
        // insert gallery images
        for (const img of this.data.gallery) {
          this.galleryImages.push({small: img.name, medium: img.name, big: img.name});
        }

        this.galleryOptions = [
          // refer to https://github.com/lukasz-galka/ngx-gallery
          // RWD settings
          {
            width: '100%',
            height: '300px',
            thumbnailsColumns: 4,
            imageAnimation: NgxGalleryAnimation.Slide
          },
          {
            breakpoint: 768,
            width: '100%',
            height: '600px',
            imagePercent: 80,
            thumbnailsPercent: 20,
            thumbnailsMargin: 20,
            thumbnailMargin: 20
          },
          {
            breakpoint: 400,
            preview: false
          }
        ];


      });


    });




  }

  getRelatedProducts() {
    this.relatedProducts = this.dataService.getRelatedProductsByCategory(
        this.data.id,
        this.data.category.name,
        4
    );
  }

  dropdownOnChange(event: Options) {
    console.log('dropdown value', event);
    this.option = event;
  }

  quantityOnChange(event: number) {
    console.log('quantity value', event);
    this.quantity = event;
  }

  addToCart() {
    this.dataService.addShoppingCartItem({
      product: this.data,
      quantity: this.quantity,
      option: this.option
    });
  }

  scrollToTop() {
    window.scroll(0, 0);
  }
}
