import {Component, OnInit} from '@angular/core';
import {DataService} from '../../services/data.service';
import {CategoryInfo} from '../../interface/ec-template.interface';
import {CategoryService} from '../../services/category.service';

@Component({
  selector: 'app-category-sidebar',
  templateUrl: './category-sidebar.component.html',
  styleUrls: ['./category-sidebar.component.scss']
})
export class CategorySidebarComponent implements OnInit {
  categoryList: CategoryInfo[];

  constructor(public dataService: CategoryService) {
  }

  ngOnInit() {
    this.dataService.findAllCategorias().subscribe((data: CategoryInfo[]) => {
      this.categoryList = data;
    });
  }
}
