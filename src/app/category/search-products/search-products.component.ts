import { Component, OnInit } from '@angular/core';
import {Product} from '../../interface/ec-template.interface';
import {ActivatedRoute} from '@angular/router';
import {DataService} from '../../services/data.service';
import {ProductService} from '../../services/product.service';

@Component({
  selector: 'app-search-products',
  templateUrl: './search-products.component.html',
  styleUrls: ['./search-products.component.scss']
})
export class SearchProductsComponent implements OnInit {
  productList: Product[];
  id: string;
  category: '';

  constructor(private route: ActivatedRoute, public dataService: DataService, private service: ProductService) {
  }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.service.findProductsByName(params['produto']).subscribe((data) => {
        this.productList = data;

      });
    });


  }
}
