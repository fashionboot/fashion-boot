import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {ProductService} from '../../services/product.service';
import {Product} from '../../interface/ec-template.interface';
import {DataService} from '../../services/data.service';

@Component({
    selector: 'app-marca-list',
    templateUrl: './marca-list.component.html',
    styleUrls: ['./marca-list.component.scss']
})
export class MarcaListComponent implements OnInit {
    productList: Product[];
    id: string;
    category: '';

    constructor(private route: ActivatedRoute, public dataService: DataService, private service: ProductService) {
    }


    ngOnInit() {
        //this.sub = this.route.snapshot.paramMap.get("sub")!;
        this.route.params.subscribe(params => {
            this.service.getProductListByMarca(params['marca']).subscribe((data) => {
                this.productList = data;

            });
        });



    }
}
