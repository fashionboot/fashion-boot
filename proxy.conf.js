const fs = require('fs');

const proxy = [
    {
        context: ['/backend'],
        target: 'http://localhost:8081',
        pathRewrite: {'^/backend' : ''},
        secure: false,
        logLevel: 'debug',
        changeOrigin: true
    }
];
module.exports = proxy;
